#include<stdio.h>
#include<string.h>
#include<ctype.h>
char keywords[][10]={"int","for","while","do","void","main","char","float","main"};
char operators[]="+-*/%=";
char seperators[]="(){};,";
int numk=8,num_seperators=6;

int isOperator(char ch)
{
    int i;
    for(i=0;i<strlen(operators);i++)
    {
        if(ch==operators[i])
            return 1;
    }
    return 0;
}

int isSeperator(char ch)
{
    int i;
    for(i=0;i<strlen(seperators);i++)
    {
        if(ch==seperators[i])
            return 1;
    }
    return 0;
}

int iskeyWord(char *ch)
{
    int i;
    for(i=0;i<numk;i++)
    {
        if(strcmp(ch,keywords[i])==0)
            return 1;
    }
    return 0;
}
void main()
{
    char ch;
    FILE *input,*table;
    input = fopen("input.c","r");
    table = fopen("table.txt","w");
    ch=fgetc(input);
    while(ch!=EOF)
    {
        
        if(isdigit(ch))
        {
            int temp=ch-'0';
            ch=fgetc(input);
            while(isdigit(ch))
            {
                temp*=10+(ch-'0');
                 ch=fgetc(input);
            }
            
         fprintf(table,"%d\tNumber\n",temp);   
        }
        if(isalnum(ch))
        {
            char t[20],j=0;
            t[j++]=ch;
            ch=fgetc(input);
            while(isalnum(ch))
            {
                t[j++]=ch;
                ch=fgetc(input);
            }
            if(ch==' '||ch=='\n'|| isSeperator(ch)|| isOperator(ch))
            {
                t[j++]='\0';
                
            }
        
            if(iskeyWord(t)==1)
            {
                 fprintf(table,"%s\tKeyword\n",t);    
            }
            else
            {
                fprintf(table,"%s\tIdentifier\n",t);   
            }
        }
        if(isOperator(ch)==1)
        {
            fprintf(table,"%c\tOperator\n",ch);
        }
        if(isSeperator(ch)==1)
        {
            fprintf(table,"%c\t%c\n",ch,ch);
        }
    ch=fgetc(input);
    }
     fclose(input);
     fclose(table);
     table = fopen("table.txt","r");
     printf("\nLexemes\tTokens\n");
     char out1[30],out2[30];
     fscanf(table,"%s%s",out1,out2);
     while(!feof(table))
     {
         printf("%s\t%s\n",out1,out2);
         fscanf(table,"%s%s",out1,out2);

     }


}